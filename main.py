from genericpath import exists
import tkinter as tk

width_windows = 80
height_windows = 60
scale = 10
matrix = [[0 for _ in range(width_windows)] for _ in range(height_windows)]
time = 200


def init_window(window):
    height = height_windows * scale
    width = width_windows * scale
    window.resizable(False, False)
    window.title("Game Of Life")
    window.geometry(str(width) + "x" + str(height) + "+0+0")

def init_matrix(canvas):
    j = 0
    for y in matrix:
        i = 0
        for x in y:
            canvas.create_rectangle(i * scale, j * scale, (i * scale) + scale, (j * scale) + scale, fill="#000")
            i += 1
        j += 1

def init_game(canvas):
    init_matrix(canvas)
    #vessel
    matrix[2][1]=1
    color_cell(2, 1, 1)
    matrix[3][2]=1
    color_cell(3, 2, 1)
    matrix[1][3]=1
    color_cell(1, 3, 1)
    matrix[2][3]=1
    color_cell(2, 3, 1)
    matrix[3][3]=1
    color_cell(3, 3, 1)
 

def color_cell(i, j, state):
    if state == 0:
        canvas.create_rectangle(i * scale, j * scale, (i * scale) + scale, (j * scale) + scale, fill="#000")
    else:
        canvas.create_rectangle(i * scale, j * scale, (i * scale) + scale, (j * scale) + scale, fill="#FFF")


def update_game():
    matrix_temp = [[0 for _ in range(width_windows)] for _ in range(height_windows)]
    j = 0
    for y in matrix:
        i = 0
        for x in y: 
            if len(matrix) > i + 1:
                left = matrix[i - 1][j]
                right = matrix[i + 1][j]
                bottom = matrix[i][j + 1]
                top = matrix[i][j - 1]
                left_top = matrix[i - 1][j - 1]            
                left_bottom = matrix[i - 1][j + 1]
                right_top = matrix[i + 1][j - 1]
                right_bottom = matrix[i + 1][j + 1]

                nb_alive = top + bottom + left + right + left_top + left_bottom + right_top + right_bottom
                
                if nb_alive == 3:
                    matrix_temp[i][j] = 1
                elif matrix[i][j] == 1 and nb_alive == 2:
                    matrix_temp[i][j] = 1
                else:
                    matrix_temp[i][j] = 0
            i += 1
        j += 1
    update_matrix(matrix_temp)

def update_matrix(matrix_temp):
    j = 0
    for y in matrix_temp:
        i = 0
        for x in y: 
            if len(matrix_temp) > i + 1:
                matrix[i][j] = matrix_temp[i][j]
                color_cell(i, j, matrix_temp[i][j])
            i += 1
        j += 1
    
    window.after(time, update_game)


window = tk.Tk()
init_window(window)

canvas = tk.Canvas(window, width=width_windows * scale, height=height_windows * scale, bg="#FF0000")
canvas.pack()
init_game(canvas)
window.after(time, update_game)
window.mainloop()
